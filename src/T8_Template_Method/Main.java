package T8_Template_Method;

public class Main {

	public static void main(String[] args) {
		System.out.println("Pelataan numeron-arvaus peliä.\nAnna pelaajien lukumäärä:");
		int pelaajat = Lue.kluku();
		Game game = new NumberGuess();
		game.playOneGame(pelaajat);
	}

}
