package T8_Template_Method;

import java.util.concurrent.ThreadLocalRandom;

public class NumberGuess extends Game{

	private String voittaja;
	private int arvaa;
	
	@Override
	void initializeGame() {
		System.out.println("Se kuka arvaa ensin numeron 1 ja 10 väliltä voittaa.");
		System.out.println("Pelaajien lukumäärä: "+playersCount);
		arvaa = ThreadLocalRandom.current().nextInt(1, 11);
	}

	@Override
	void makePlay(int player) {
		player +=1;
		System.out.println("Pelaajan "+player+" vuoro. Valitse numero:");
		int arvaus = Lue.kluku();
		if(arvaus == arvaa) {
			voittaja = "Pelaaja numero "+player;
		}
		else {
			System.out.println("Väärin meni!");
		}
		
	}

	@Override
	boolean endOfGame() {
		if(voittaja == null) {
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	void printWinner() {
		System.out.println("Pelin voitti "+voittaja+"!\nOnneksi olkoon!");
	}

}
