package T7_State;

public class Trainer {

	private Pokemon state;
	
	public Trainer() {
		state = new Squirtle(this);
	}
	
	protected void evolve(Pokemon state) {
		this.state = state;
	}
	
	public void attack() {
		state.attack(this);
	}
	public void escape() {
		state.escape();
	}
	public void info() {
		state.info();
	}
}
