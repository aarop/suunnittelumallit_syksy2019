package T7_State;

abstract class Pokemon {

	protected Trainer trainer;
	
	protected int level;
	
	public Pokemon(Trainer t) {
		trainer = t;
	}
	
	public void attack(Trainer t) {}
	
	public void escape() {}
	
	public void evolve(Trainer t, Pokemon p) {
		t.evolve(p);
	}
	
	public void info() {
		
	}
	
	
	
}
