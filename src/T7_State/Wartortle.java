package T7_State;

import java.util.concurrent.ThreadLocalRandom;

public class Wartortle extends Pokemon{

	public Wartortle(Trainer t) {
		super(t);
	}

	public void attack(Trainer t) {
		int rand = ThreadLocalRandom.current().nextInt(0, 3);
		System.out.println(rand);
		switch(rand) {
		  case 0:
			  System.out.println("Wartortle ampuu vastustajaansa päähän. Vastustaja kuoli.");
			  level++;
			  break;
		  case 1:
			  System.out.println("Wartortle leikkaa vastustajansa vesisäteellä puoliksi. Voitit.");
			  level++;
			  break;
		  case 2:
			  System.out.println("Ammuit ohi.");
		}
		if(level>10) {
			this.evolve(trainer);
		}
	}
	
	public void evolve(Trainer t) {
		System.out.println("Ohoh, wartortlesi kehittyy!!!");
		t.evolve(new Blastoise(t));
		System.out.println("Wartortlestasi kehittyi Blastoise! Onnea!");
	}
	
	public void escape() {
		System.out.println("Wartortle ottaa taskustaan savupommin ja pakenee paikalta sen turvin.");
	}
	
	public void info() {
		System.out.println("Pokemonisi on Wartortle, sotakilpikonna.");
	}
}
