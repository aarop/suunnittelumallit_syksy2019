package T20_Iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Main {
	
	public class Säie extends Thread{
		Iterator itr;
		ArrayList<Integer> lista;
		public void setLista(ArrayList<Integer> lista) {
			this.lista = lista;
		}
		public void setIterator(Iterator i) {
			itr = i;
		}
		public void run() {
			if(itr == null)itr = lista.iterator();
			while(itr.hasNext()) {
				System.out.println(itr.next());
			}
		}
	}
	public class Säie2 extends Thread{
		Iterator itr;
		ArrayList<Integer> lista;
		public void setLista(ArrayList<Integer> lista) {
			this.lista = lista;
		}
		public void setIterator(Iterator i) {
			itr = i;
		}
		public void run() {
			if(itr == null)itr = lista.iterator();
			while(itr.hasNext()) {
				//System.out.println(itr.next());
				itr.next();
				itr.remove();
			}
		}
	}
	
	public ArrayList<Integer> newList() {
		ArrayList<Integer> lista = new ArrayList<>();
		lista.add(11);
		for(int i = 0; i<10000; i++) {
			lista.add(ThreadLocalRandom.current().nextInt(0, 999));
		}
		return lista;
	}

	public static void main(String[] args) {

		Main main = new Main();
		ArrayList<Integer> lista = main.newList();

		boolean jatka = true;
		Säie eka = main.new Säie();
		Säie toka = main.new Säie();
		Iterator itr = lista.iterator();
		while(jatka) {
			System.out.println("1 : Iteroi listaa kahden säikeen eri iteraattoreilla\n2 : Iteroi listaa kahden säikeen samalla iteraattorilla");
			System.out.println("3 : Tee listaan muutos iteroinnin aikana\n4 : Hae iteroitavan listan koko samaan aikaan kun iteraattori käy sitä läpi ja poistaa sen jäseniä");
			System.out.println("5 : Lopeta");
			int luku = Lue.kluku();
			switch(luku) {
				case 1:
					lista = main.newList();
					eka = main.new Säie();
					toka = main.new Säie();
					eka.setLista(lista);
					toka.setLista(lista);
					eka.start();
					toka.start();
					break;
				case 2:
					lista = main.newList();
					eka = main.new Säie();
					toka = main.new Säie();
					eka.setLista(lista);
					toka.setLista(lista);
					itr = lista.iterator();
					eka.setIterator(itr);
					toka.setIterator(itr);
					eka.start();
					toka.start();
					break;
				case 3:
					//Heittää ConcurrentModificationExceptionin
					lista = main.newList();
					itr = lista.iterator();
					while(itr.hasNext()) {
						int asd = (int)itr.next();
						System.out.println(asd);
						if(asd == 11) {
							lista.remove(0);
						}
					}
					break;
				case 4:
					lista = main.newList();
					int alku = lista.size();
					Säie2 poisto = main.new Säie2();
					poisto.setLista(lista);
					poisto.start();
					try {
						TimeUnit.MILLISECONDS.sleep(1);
					} catch (InterruptedException e) {
						System.out.println("Javan vika, älä syytä ittees");
						e.printStackTrace();
					}
					int loppu = lista.size();
					System.out.println("Listan pituus alussa = "+alku);
					System.out.println("Listan pituus lopussa = "+loppu+" (Pitäisi olla 0)");
				case 5:
					jatka = false;
					break;
				default:
					System.out.println("Tuntematon merkki!");
			}
		}
	}
}
