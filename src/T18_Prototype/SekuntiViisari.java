package T18_Prototype;

public class SekuntiViisari implements Cloneable{

	int sekunti;
	
	public int getSekunti() {
		return sekunti;
	}

	public void setSekunti(int sekunti) {
		this.sekunti = sekunti;
	}

	public SekuntiViisari() {
		sekunti =0;
	}
	
	public Object clone() {
		try {
			return (SekuntiViisari)super.clone();
		}catch(CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
