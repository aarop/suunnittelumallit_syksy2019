package T18_Prototype;

public class TuntiViisari implements Cloneable{

	int tunti;
	
	public TuntiViisari() {
		tunti =0;
	}

	public int getTunti() {
		return tunti;
	}

	public void setTunti(int tunti) {
		this.tunti = tunti;
	}
	
	public Object clone() {
		try {
			return (TuntiViisari)super.clone();
		}catch(CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
