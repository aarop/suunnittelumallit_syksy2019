package T18_Prototype;

public class MinuuttiViisari implements Cloneable{

	int minuutti;
	
	public MinuuttiViisari() {
		minuutti =0;
	}

	public int getMinuutti() {
		return minuutti;
	}

	public void setMinuutti(int minuutti) {
		this.minuutti = minuutti;
	}
	
	public Object clone() {
		try {
			return (MinuuttiViisari)super.clone();
		}catch(CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
