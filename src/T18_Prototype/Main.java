package T18_Prototype;


public class Main {

	public static void main(String[] args) {

		SekuntiViisari s = new SekuntiViisari();
		MinuuttiViisari m = new MinuuttiViisari();
		TuntiViisari h = new TuntiViisari();
		
		Kello klo = new Kello(s, m, h);
		//klo.start();
		Kello matala=null;
		Kello syvä=null;
		int tunti=0;
		int min=0;
		int sek=0;
		
		boolean jatka = true;
		while(jatka) {
			System.out.println("1 : Näytä aika\n2 : Aseta aika");
			System.out.println("3 : Tee kellosta syväkopio\n4 : Aseta syväkopion aika");
			System.out.println("5 : Näytä syväkopion aika\n6 : Lopeta");
			int luku = Lue.kluku();
			switch(luku) {
				case 1:
					klo.showTime();
					break;
				case 2:
					System.out.println("Anna asetettavat tunnit:");
					tunti = Lue.kluku();
					System.out.println("Anna asetettavat minuutit:");
					min = Lue.kluku();
					System.out.println("Anna asetettavat sekunnit:");
					sek = Lue.kluku();
					klo.setTime(tunti, min, sek);
					break;
				case 3:
					syvä = klo.clone();
					break;
				case 4:
					System.out.println("Anna asetettavat tunnit:");
					tunti = Lue.kluku();
					System.out.println("Anna asetettavat minuutit:");
					min = Lue.kluku();
					System.out.println("Anna asetettavat sekunnit:");
					sek = Lue.kluku();
					syvä.setTime(tunti, min, sek);
					break;
				case 5:
					syvä.showTime();
					break;
				case 6:
					jatka = false;
					break;
				default:
					System.out.println("Tuntematon merkki!");
			}
		}
	}
}
