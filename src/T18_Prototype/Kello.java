package T18_Prototype;

import java.util.concurrent.TimeUnit;

public class Kello implements Cloneable{

	private SekuntiViisari sekunti;
	private MinuuttiViisari minuutti;
	private TuntiViisari tunti;
	
	public Kello(SekuntiViisari s, MinuuttiViisari m, TuntiViisari h) {
		sekunti = s;
		minuutti = m;
		tunti = h;
	}
//	public Kello matalaKopio() {
//		try {
//			return (Kello)super.clone();
//		}catch(CloneNotSupportedException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
	public void setTime(int tunt, int min, int sek) {
		sekunti.setSekunti(sek);
		minuutti.setMinuutti(min);
		tunti.setTunti(tunt);
	}
	public Kello clone() {
		Kello klo = null;
		try {
			klo = (Kello) super.clone();
			klo.sekunti = (SekuntiViisari)sekunti.clone();
			klo.minuutti = (MinuuttiViisari)minuutti.clone();
			klo.tunti = (TuntiViisari)tunti.clone();
		}catch(CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return klo;
	}
	public void run() {
		int s =0;
		int m =0;
		int h=0;
		while(true) {
			sekunti.setSekunti(s++);
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				System.out.println("Javan vika, älä syytä ittees");
				e.printStackTrace();
			}
			if(sekunti.getSekunti()==60) {
				minuutti.setMinuutti(m++);
				sekunti.setSekunti(s=0);
				if(minuutti.getMinuutti()==60) {
					minuutti.setMinuutti(m=0);
					tunti.setTunti(h++);
					if(tunti.getTunti()==24) {
						tunti.setTunti(h=0);
					}
				}
			}
		}
	}
	public void showTime() {
		System.out.println(tunti.getTunti()+":"+minuutti.getMinuutti()+"."+sekunti.getSekunti());
	}
}
