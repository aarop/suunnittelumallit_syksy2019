package T13_Visitor;

import java.util.concurrent.ThreadLocalRandom;

public class Squirtle extends Pokemon{
	
	public Squirtle(Trainer t, IPokemonVisitor v) {
		super(t, v);
	}
	
//	@Override
//	public void accept(IPokemonVisitor visitor) {
//		visitor.visit(this);
//	}

	public void attack(Trainer t) {
		int rand = ThreadLocalRandom.current().nextInt(0, 3);
		switch(rand) {
		  case 0:
			  System.out.println("Squirtle lyö vastustajaansa nyrkillä päähän.");
			  level++;
			  break;
		  case 1:
			  System.out.println("Squirtle potkaisee vastustajaansa sääreen.");
			  level++;
			  break;
		  case 2:
			  System.out.println("Huti meni ähä ähä.");
		}
		super.visitor.visit(this);
	}
	
	public void evolve(Trainer t) {
		System.out.println("Ohoh, squirtle kehittyy!!!");
		t.evolve(new Wartortle(t, visitor));
		System.out.println("Squirtlestasi kehittyi Wartortle! Onnea!");
	}
	
	public void escape() {
		System.out.println("Pocket sand!\nSquirtle heittää taskunpohjan hiekkansa vastustajansa silmiin ja pakenee paikalta.");
	}
	
	public void info() {
		System.out.println("Pokemonisi on Squirtle, kilpikonnaorava.");
		System.out.println("Level: "+level);
	}

}
