package T13_Visitor;

public class SquirtleVisitor implements IPokemonVisitor{

	@Override
	public void visit(Squirtle sq) {
		if (sq.level > 5) {
			sq.evolve(sq.trainer);
		}
	}

	@Override
	public void visit(Wartortle wa) {
		if (wa.level > 7) {
			wa.evolve(wa.trainer);
		}
	}

	@Override
	public void visit(Blastoise bla) {
		if (bla.level > 10) {
			bla.evolve(bla.trainer);
		}
	}

}
