package T13_Visitor;

import java.util.concurrent.ThreadLocalRandom;

public class Blastoise extends Pokemon{

	public Blastoise(Trainer t, IPokemonVisitor v) {
		super(t, v);
	}
	
//	@Override
//	public void accept(IPokemonVisitor visitor) {
//		visitor.visit(this);
//	}

	public void attack(Trainer t) {
		int rand = ThreadLocalRandom.current().nextInt(0, 3);
		switch(rand) {
		  case 0:
			  System.out.println("Blastoise räjäyttää vastustajansa naaman irti. Brutaalia.");
			  level++;
			  break;
		  case 1:
			  System.out.println("Blastoise pieraisee kohti vastustajaa. Vastustajasi kuoli.");
			  level++;
			  break;
		  case 2:
			  System.out.println("Blastoise ampui huti ja tuhosi vahingossa orpokodin.");
		}
		visitor.visit(this);
	}
	
	public void evolve(Trainer t) {
		System.out.println("Blastoise on kehitysateen huipulla mutta yrittää kehittyä lisää. Osta steroideja.");
	}
	
	public void escape() {
		System.out.println("Blastoise lentää karkuun vesitykeillään.");
	}
	
	public void info() {
		System.out.println("Pokemonisi on Blastoise, Super-Soaker-kilpikonna.");
		System.out.println("Level: "+level);
	}

}
