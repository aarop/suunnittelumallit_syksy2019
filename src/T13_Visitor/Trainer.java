package T13_Visitor;

public class Trainer {

	private Pokemon state;
	
	private IPokemonVisitor visitor;
	
	public Trainer() {
		visitor = new SquirtleVisitor();
		state = new Squirtle(this, visitor);
	}
	
	protected void evolve(Pokemon state) {
		this.state = state;
	}
	
	public void attack() {
		state.attack(this);
	}
	public void escape() {
		state.escape();
	}
	public void info() {
		state.info();
	}
}
