package T13_Visitor;

abstract class Pokemon {

	protected Trainer trainer;
	
	protected IPokemonVisitor visitor;
	
	protected int level;
	
	public Pokemon(Trainer t, IPokemonVisitor v) {
		trainer = t;
		visitor = v; 
	}
	
	public void attack(Trainer t) {}
	
	public void escape() {}
	
	public void evolve(Trainer t, Pokemon p) {
		t.evolve(p);
	}
	
	public void info() {}
	
	//public abstract void accept(IPokemonVisitor visitor);
	
}
