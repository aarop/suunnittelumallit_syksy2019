package T13_Visitor;

public class Main {

	public static void main(String[] args) {
		Trainer t = new Trainer();
		boolean jatka = true;
		System.out.println("Aloitetaas peli.");
		while(jatka) {
			System.out.println("\nKirjoita:\nH hyökätäksesi\nP paetaksesi\nI saadaksesi pokemonin tietoja ja\nQ poistuaksesi pelistä.\n");
			System.out.println("Tielläsi seisoo vihollinen.");
			System.out.println("Tee valintasi");
			char kirjain = Lue.merkki();
			switch(kirjain) {
				case 'h':
				case 'H':
					t.attack();
					break;
				case 'p':
				case 'P':
					t.escape();
					break;
				case 'i':
				case 'I':
					t.info();
					break;
				case 'q':
				case 'Q':
					System.out.println("Heido!");
					jatka = false;
					break;
				default:
					System.out.println("Tuntematon merkki!");
			}
		}
		
		
		
		
	}

}
