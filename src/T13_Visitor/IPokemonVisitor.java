package T13_Visitor;

public interface IPokemonVisitor {

	void visit(Squirtle sq);
	
	void visit(Wartortle wa);
	
	void visit(Blastoise bla);
}
