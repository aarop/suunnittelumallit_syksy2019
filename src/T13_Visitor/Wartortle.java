package T13_Visitor;

import java.util.concurrent.ThreadLocalRandom;

public class Wartortle extends Pokemon{

	public Wartortle(Trainer t, IPokemonVisitor v) {
		super(t, v);
	}
	
//	@Override
//	public void accept(IPokemonVisitor visitor) {
//		visitor.visit(this);
//	}

	public void attack(Trainer t) {
		int rand = ThreadLocalRandom.current().nextInt(0, 3);
		switch(rand) {
		  case 0:
			  System.out.println("Wartortle ampuu vastustajaansa päähän. Vastustaja kuoli.");
			  level++;
			  break;
		  case 1:
			  System.out.println("Wartortle leikkaa vastustajansa vesisäteellä puoliksi. Voitit.");
			  level++;
			  break;
		  case 2:
			  System.out.println("Ammuit ohi.");
		}
		super.visitor.visit(this);
	}
	
	public void evolve(Trainer t) {
		System.out.println("Ohoh, wartortlesi kehittyy!!!");
		t.evolve(new Blastoise(t, visitor));
		System.out.println("Wartortlestasi kehittyi Blastoise! Onnea!");
	}
	
	public void escape() {
		System.out.println("Wartortle ottaa taskustaan savupommin ja pakenee paikalta sen turvin.");
	}
	
	public void info() {
		System.out.println("Pokemonisi on Wartortle, sotakilpikonna.");
		System.out.println("Level: "+level);
	}
}
