package T6_Decorator;

public abstract class SecretData implements Data{

	protected Data data;
	public SecretData(Data data) {
		this.data = data;
	}
	public void writeData(String nimi, String text) {
		data.writeData(nimi, text);
	}
	public String getData() {
		return data.getData();
	}
}
