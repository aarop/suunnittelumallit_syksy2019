package T6_Decorator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class UnprotectedData implements Data{

	private String nimi;
	@Override
	public void writeData(String nimi, String text) {
		this.nimi = nimi;
		try(FileWriter tiedosto = new FileWriter(nimi+".txt");
			BufferedWriter tuloste = new BufferedWriter(tiedosto);){
				tuloste.write(text);
			}catch(IOException e) {
				System.out.println("Ei onnannu tiedostoon kirjoitus.");
				e.printStackTrace();
			}
	}

	@Override
	public String getData() {
		String pal = "ERROR";
		try(FileReader virta = new FileReader(nimi+".txt");
			BufferedReader puskuroituVirta = new BufferedReader(virta);){
				pal = puskuroituVirta.readLine();
			}catch(IOException e) {
				System.out.println("Ei onnistunu tiedoston lukeminen.");
				e.printStackTrace();
			}
		return pal;
	}

}
