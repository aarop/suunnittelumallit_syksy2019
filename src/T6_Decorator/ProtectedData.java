package T6_Decorator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class ProtectedData extends SecretData{

	private String nimi;
	
	public ProtectedData(Data data) {
		super(data);
	}
	
	@Override
	public void writeData(String nimi, String text) {
		this.nimi = nimi;
		try(FileWriter tiedosto = new FileWriter(nimi+"_SALATTU.txt");
			BufferedWriter tuloste = new BufferedWriter(tiedosto);){
				String salaa = encrypt(text);
				tuloste.write(salaa);
			}catch(Exception e) {
				System.out.println("Ei onnannu tiedostoon kirjoitus.");
				e.printStackTrace();
			}
	}

	@Override
	public String getData() {
		String pal = "ERROR";
		try(FileReader virta = new FileReader(nimi+"_SALATTU.txt");
			BufferedReader puskuroituVirta = new BufferedReader(virta);){
				String read = puskuroituVirta.readLine();
				pal = decrypt(read);
			}catch(IOException e) {
				System.out.println("Ei onnistunu tiedoston lukeminen.");
				e.printStackTrace();
			}
		return pal;
	}

	//Salaushommelit
	private static String myKey = "salasana123";
	private static SecretKeySpec secretKey;
    private static byte[] key;
 
    public static void setKey()
    {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
 
    public static String encrypt(String strToEncrypt)
    {
        try
        {
            setKey();
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        }
        catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
 
    public static String decrypt(String strToDecrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
	public String getPlainData() {
		String pal = "ERROR";
		try(FileReader virta = new FileReader(nimi+"_SALATTU.txt");
			BufferedReader puskuroituVirta = new BufferedReader(virta);){
				pal = puskuroituVirta.readLine();
			}catch(IOException e) {
				System.out.println("Ei onnistunu tiedoston lukeminen.");
				e.printStackTrace();
			}
		return pal;
	}
}
