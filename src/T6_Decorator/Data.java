package T6_Decorator;

public interface Data {

	public void writeData(String nimi, String text);
	public String getData();
}
