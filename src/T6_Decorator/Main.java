package T6_Decorator;

public class Main {

	public static void main(String[] args) {
		Data sData = new ProtectedData((new UnprotectedData()));
		Data data = new UnprotectedData();
		System.out.println("Kirjoitan kaksi tiedostoa, toinen salattuna ja toinen salamattomana.\n");
		System.out.println("Anna tiedostoon kirjoitettava teksti:");
		String teksti = Lue.rivi();
		System.out.println("Anna vielä tiedoston nimi:");
		String nimi = Lue.rivi();
		data.writeData(nimi, teksti);
		sData.writeData(nimi, teksti);
		System.out.println("Suojattu data: "+ sData.getData());
		System.out.println("Suojattu data vielä salatussa muodossa: "+ ((ProtectedData) sData).getPlainData());
		System.out.println("Ei suojattu data: "+ data.getData());
	}

}
