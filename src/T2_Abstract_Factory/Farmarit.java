package T2_Abstract_Factory;

public class Farmarit {

	private String merkki;
	
	public Farmarit(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
