package T2_Abstract_Factory;

public class Kengät {

	private String merkki;
	
	public Kengät(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
