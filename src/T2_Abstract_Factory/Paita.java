package T2_Abstract_Factory;

public class Paita {

	private String merkki;
	
	public Paita(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
