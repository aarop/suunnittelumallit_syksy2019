package T2_Abstract_Factory;

public interface IVaateTehdas {

	
	public abstract Lippalakki pueLippis();
	
	public abstract Kengät pueKengät();
	
	public abstract Farmarit pueFarmarit();
	
	public abstract Paita puePaita();
	
	public abstract void luettele();
}
