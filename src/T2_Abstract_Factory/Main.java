package T2_Abstract_Factory;

public class Main {

	public static void main(String[] args) {
		//IVaateTehdas tehdas = new AdidasTehdas();
		IVaateTehdas tehdas = new BossTehdas();
		tehdas.pueKengät();
		tehdas.pueFarmarit();
		tehdas.pueLippis();
		tehdas.puePaita();
		tehdas.luettele();
		
	}
}
