package T2_Abstract_Factory;

public class Lippalakki {

	private String merkki;
	
	public Lippalakki(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
