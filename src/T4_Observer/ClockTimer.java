package T4_Observer;

public class ClockTimer extends Subject{
	
	public int hour;
	public int minute;
	public int sec;
	
	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSec() {
		return sec;
	}
	void tick() {
		sec += 1;
		if(sec == 60) {
			minute +=1;
			sec=0;
			if(minute==60) {
				hour+=1;
				minute=0;
				if(hour==24) {
					hour=0;
				}
			}
		}
		Notify();
	}

}
