package T4_Observer;

public interface Observer {

	public abstract void update(Subject changedSubject);
}
