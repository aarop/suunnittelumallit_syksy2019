package T2_B_Factory_from_file;

public class Kengät {

	private String merkki;
	
	public Kengät(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
