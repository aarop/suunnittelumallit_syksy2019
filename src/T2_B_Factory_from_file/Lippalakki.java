package T2_B_Factory_from_file;

public class Lippalakki {

	private String merkki;
	
	public Lippalakki(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
