package T2_B_Factory_from_file;

public class BossTehdas implements IVaateTehdas {

	private Lippalakki lippis;
	private Paita paita;
	private Kengät kengät;
	private Farmarit farmarit;
	private final String merkki = "Boss";
	
	@Override
	public Lippalakki pueLippis() {
		lippis = new Lippalakki(merkki);
		return lippis;
	}

	@Override
	public Kengät pueKengät() {
		kengät = new Kengät(merkki);
		return kengät;
	}

	@Override
	public Farmarit pueFarmarit() {
		farmarit = new Farmarit(merkki);
		return farmarit;
	}

	@Override
	public Paita puePaita() {
		paita = new Paita(merkki);
		return paita;
	}
	@Override
	public void luettele() {
		System.out.println("Ylläni on: \n");
		System.out.println(paita.toString()+"- merkkinen paita, \n");
		System.out.println(kengät.toString()+"- merkkiset kengät, \n");
		System.out.println(farmarit.toString()+"- merkkiset farmarit ja \n");
		System.out.println(lippis.toString()+"- merkkinen lippis.");
	}

}
