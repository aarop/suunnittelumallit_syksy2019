package T2_B_Factory_from_file;

public class Farmarit {

	private String merkki;
	
	public Farmarit(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
