package T2_B_Factory_from_file;

public class Paita {

	private String merkki;
	
	public Paita(String merkki) {
		this.merkki = merkki;
	}
	
	public String toString() {
		return merkki;
	}
}
