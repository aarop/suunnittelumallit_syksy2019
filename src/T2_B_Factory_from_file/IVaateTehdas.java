package T2_B_Factory_from_file;

public interface IVaateTehdas {

	
	public abstract Lippalakki pueLippis();
	
	public abstract Kengät pueKengät();
	
	public abstract Farmarit pueFarmarit();
	
	public abstract Paita puePaita();
	
	public abstract void luettele();
}
