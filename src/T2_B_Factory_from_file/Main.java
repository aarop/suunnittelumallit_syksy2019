package T2_B_Factory_from_file;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

	public static void main(String[] args) {
		IVaateTehdas tehdas = null;
		Class c = null;
		Properties properties = new Properties();
		try {
			properties.load(
				new FileInputStream("src/T2_B_Factory_from_file/tehdas.properties"));
		} catch (IOException e) {e.printStackTrace();}
		try{
			c = Class.forName(properties.getProperty("tehdas"));
			tehdas = (IVaateTehdas)c.newInstance();
		}catch (Exception e){e.printStackTrace();}
		
		tehdas.pueKengät();
		tehdas.pueFarmarit();
		tehdas.pueLippis();
		tehdas.puePaita();
		tehdas.luettele();
	}
}
