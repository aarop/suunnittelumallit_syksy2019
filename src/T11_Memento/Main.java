package T11_Memento;

public class Main {

	public static void main(String[] args) {
		boolean jatka = true;
		System.out.println("Peli alkaa.");
		Asiakas as1 = new Asiakas();
		Arvuuttaja arvuuttaja = new Arvuuttaja();
		as1.memento = arvuuttaja.liityPeliin(as1);
		while(jatka) {
			System.out.println("Arvaa numero 1-10");
			int arvaus = Lue.kluku();
			if(arvuuttaja.arvaa(arvaus, as1.memento)) {
				System.out.println("Oikein!");
				jatka = false;
			}
			else {
				System.out.println("Väärin.");
			}
		}
	}

}
