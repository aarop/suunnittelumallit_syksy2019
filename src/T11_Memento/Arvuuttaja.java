package T11_Memento;

import java.util.concurrent.ThreadLocalRandom;

public class Arvuuttaja {
	
	public Object liityPeliin(Asiakas as) {
		int rand = ThreadLocalRandom.current().nextInt(0, 11);
		return new Memento(rand, as);
	}
	public boolean arvaa(int arvaus, Object obj) {
		Memento memento = (Memento)obj;
		if(arvaus == memento.luku) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public class Memento {

		private Asiakas state;
		public int luku;
		
		public Memento(int luku, Asiakas state) {
			this.luku = luku;
			this.state = state;
		}
		
//		public Asiakas getState() {
//			return state;
//		}
//		
//		public Object save() {
//			return new Memento(this.luku, this.state);
//		}
//		public void undo(Object obj) {
//			Memento memento = (Memento)obj;
//			this.luku = memento.luku;
//			this.state = memento.state;
//		}
	}
}
