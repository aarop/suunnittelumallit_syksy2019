package T9_Strategy;

import java.util.Random;

public class Menu {
//main alkaa-----------------------------------------------------------------------------
        public static void main(String[] args) {
        	printMenu();
        }
//main loppuu --------------------------------------------------------------------------
//printMenu alkaa------------------------------------------------------------------
        private static void printMenu() {
        		Algoritmi alg = new SelectSort();
        		User user = new User(alg);
                char select;
                do {
                    System.out.println("\n\t\t\t1. Luo/sekoita lista.");
                    System.out.println("\t\t\t2. Järjestä ja luettele lista selectSort algoritmilla.");
                    System.out.println("\t\t\t3. Järjestä ja luettele lista mergeSort algoritmilla.");
                    System.out.println("\t\t\t4. Järjestä ja luettele lista quickSort algoritmilla.");
                    System.out.println("\t\t\t5. lopetus ");
                    select = Lue.merkki();
                    switch (select) {
	                    case '1':
	                        System.out.println("Anna alkioiden määrä:");
	                        int nro = Lue.kluku();
	                        user.sekoitaLista(nro);
	                        break;
	                    case '2':
	                    	alg = new SelectSort();
	                    	user.setAlgoritmi(alg);
	                    	user.lajittele();
	                        break;
	                    case '3':
	                    	alg = new MergeSort();
	                    	user.setAlgoritmi(alg);
	                    	user.lajittele();
	                        break;
	                    case '4':
	                    	alg = new QuickSort();
	                    	user.setAlgoritmi(alg);
	                    	user.lajittele();
	                        break;
	                    case '5':
	                        break;
                    }
                }
                while (select != '5');
        }
        

//printMenu loppuu ----------------------------------------------------------------
}
