package T9_Strategy;

public class MergeSort implements Algoritmi{

    public static long laskuri;
    public static int MAX;
    private static int[] tau;
    
    
	@Override
	public int[] lajittele(int[] lista) {
		MAX = lista.length;
		tau = new int[MAX];
		mergeSort(lista, 0, MAX-1);
        for (int i=0;i<MAX;i++) {
            System.out.print(lista[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        System.out.println("\nLaskurin tulos lomituksessa = "+laskuri);
        System.out.println("\nKuittaa tulos, paina Enter ");
        Lue.merkki();
        return lista;
	}

     
    
    //oletus: osataulukot t[p..q] ja t[q+1...r] ovat järjestyksess„
    public static int[] merge(int t[], int p, int q, int r) {
        //i osoittaa 1. osataulukkoa, j osoittaa 2. osataulukkoa
        // k osoittaa aputaulukkoa, johon yhdiste kirjoitetaan.
        int i=p, j=q+1, k=0;
        laskuri += 2;
        while(i<q+1 && j<r+1) {
        	laskuri++;
            if (t[i]<t[j]) {
                    tau[k++]=t[i++];
            }
            else {
                    tau[k++]=t[j++];
            }
            laskuri+=2;
        }
        //toinen osataulukko käsitelty, siirretään toisen käsittelemättömät
        laskuri++;
        while (i<q+1) {
                tau[k++]=t[i++];
        		laskuri++;
        }
        laskuri++;
        while (j<r+1) {
        	tau[k++]=t[j++];
        	laskuri++;
        }
        laskuri++;
        //siirretään yhdiste alkuperäiseen taulukkoon
        for (i=0;i<k;i++) {
                t[p+i]=tau[i];
                laskuri++;
        }
        return t;
    }

    public static int[] mergeSort(int t[],  int alku,  int loppu) {
    	int[] lista = new int[loppu+1];
        int ositus;
        long la, ll, lt;
        if (alku<loppu) { //onko väh. 2 alkiota, että voidaan suorittaa ositus

                la=alku; ll=loppu;
                lt=(la+ll)/2;
                ositus=(int)lt;
                mergeSort(t, alku, ositus);//lajitellaan taulukon alkupää
                mergeSort(t, ositus+1, loppu);//lajitellaan taulukon loppupää
                lista = merge(t, alku, ositus, loppu);//yhdistetään lajitellut osataulukot
        }
        return lista;

    }
    
}
