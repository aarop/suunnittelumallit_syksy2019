package T9_Strategy;

public class SelectSort implements Algoritmi{

    public static long laskuri;
    
	@Override
	public int[] lajittele(int[] taul) {
		int MAX = taul.length;
		int i, j, k, apu, pienin;
        laskuri++; //Ulompi for lause alla
        for (i=0;i<MAX;i++) {
            pienin=i;
            laskuri++;
            for (j=i+1;j<MAX;j++) {
            	laskuri++;
                /* löytyykö taulukon loppupäästä pienempää alkiota? */
                if (taul[j] < taul[pienin]) {
                    pienin=j;
                }
                laskuri++;
            }
            laskuri++;
            if (pienin != i) {
                /* jos löytyi suoritetaan alkioiden vaihto */
                apu=taul[pienin];
                taul[pienin]=taul[i];
                taul[i]=apu;
            }
            laskuri++;
        }
        System.out.println();
        for (i=0;i<MAX;i++) {
            System.out.print(taul[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        System.out.println("\nLaskuri = "+ laskuri);
        System.out.println("\nKuittaa tulos, paina Enter ");
        Lue.merkki();
        return taul;
	}

}
