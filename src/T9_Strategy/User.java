package T9_Strategy;

import java.util.Random;

public class User {

    public static int[] lista;
    public static long laskuri;
    private Algoritmi alg;
    
    public User(Algoritmi a) {
    	alg = a;
    }
    public void setAlgoritmi(Algoritmi a) {
    	alg = a;
    }
    
    public void lajittele() {
    	alg.lajittele(lista);
    }
    
    public void sekoitaLista(int MAX) {
        lista = new int[MAX];
        int i;
        Random r = new Random(); //luodaan satunnaislukugeneraattori
        System.out.println("Generoidaan syöttöaineisto: ");
        for (i=0;i<MAX;i++) {
            lista[i] = r.nextInt(1000); //generoidaan luvut
            System.out.print(lista[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        System.out.println("\nKuittaa tulos, paina Enter ");
        Lue.merkki();
    }
}
