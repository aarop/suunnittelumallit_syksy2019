package T9_Strategy;

public class QuickSort implements Algoritmi{

    public static long laskuri;

	@Override
	public int[] lajittele(int[] taulukko) {
        System.out.println("\nJärjestellään käyttämällä Quicksorttia.");
        qs(taulukko, taulukko.length);
        System.out.println("Valmis");
        System.out.println("Vertailujen lukumäärä: " + laskuri);
        System.out.println("\nKuittaa tulos, paina Enter ");
        return taulukko;
	}
	
    public static void quickSort(int table[], int lo0, int hi0) {
        int lo = lo0;
        int hi = hi0;
        int mid, swap;

        mid = table[ (lo0 + hi0) / 2];
        laskuri++;
        while (lo <= hi) {
        	laskuri++;
            while (table[lo] < mid) {
                ++lo;
                laskuri++;
            }
            laskuri++;
            while (table[hi] > mid) {
                --hi;
                laskuri++;
            }
            laskuri++;
            if (lo <= hi) {
                swap = table[lo];
                table[lo] = table[hi];
                ++lo;
                table[hi] = swap;
                --hi;
            }
            laskuri++;
        }
        laskuri++;
        if (lo0 < hi) {
            quickSort(table, lo0, hi);
        }
        laskuri++;
        if (lo < hi0) {
            quickSort(table, lo, hi0);
        }
    }

    public static void qs(int table[], int values) {
        quickSort(table, 0, values - 1);

        System.out.println("\nJärjestetty aineisto:\n");
        for (int i = 0; i < values; i++) {
            System.out.print(table[i] + " ");
            if (i > 0 && i % 40 == 0) { // rivinvaihto
                System.out.println();
            }
        }
    }
    
}
