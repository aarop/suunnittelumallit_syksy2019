package T5_Singleton;

public class Main {

	public static void main(String[] args) {
		int jatka = 0;
		int kumpiVoitti = 0;
		Pelaaja pelaaja1 = new Pelaaja();
		Pelaaja pelaaja2 = new Pelaaja();
		while(jatka < 3) {
			System.out.println("Vedetään pitkää tikkua, paras kolmesta voittaa.");
			System.out.println("Pelaaja 1 aloittaa.");
			int eka = pelaaja1.getTikku();
			System.out.println("Pelaaja 2 seuraavaksi.");
			int toka = pelaaja2.getTikku();
			if(eka < toka) {
				System.out.println("Pelaaja 2 veti pitemmän korren!");
				System.out.println(toka +" isompi kuin " +eka);
				kumpiVoitti++;
			}
			else {
				System.out.println("Pelaaja 2 veti pitemmän korren!");
				System.out.println(eka +" isompi kuin " +toka);
				kumpiVoitti--;
			}
			jatka++;
		}
		if(kumpiVoitti >= 1) {
			System.out.println("Pelaaja 1 voitti!");
		}
		else {
			System.out.println("Pelaaja 2 veti pitemmät korret.");
		}

	}

}
