package T5_Singleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class Tikku {

    private static volatile Tikku instance = null;
    
    private static ArrayList<Integer> lista = new ArrayList<Integer>
    										( Arrays.asList(0, 1, 2, 3, 5, 6) );

    private Tikku() {}

    public static Tikku getInstance() {
        if (instance == null) {
            synchronized(Tikku.class) {
                if (instance == null) {
                    instance = new Tikku();
                }
            }
        }

        return instance;
    }
    
    public static int getTikku() {
    	int pituus = lista.size();
    	int rand = ThreadLocalRandom.current().nextInt(0, pituus);
    	int tikku = lista.get(rand);
    	lista.remove(rand);
    	return tikku;
    }
}
