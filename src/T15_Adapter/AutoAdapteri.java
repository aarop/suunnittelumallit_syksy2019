package T15_Adapter;

public class AutoAdapteri implements Eläin{

	Ajoneuvo auto;
	public AutoAdapteri(Ajoneuvo auto) {
		this.auto = auto;
	}

	public void ratsasta() {
		auto.aja();
	}

	public void syötä() {
		auto.tankkaa();
	}

}
