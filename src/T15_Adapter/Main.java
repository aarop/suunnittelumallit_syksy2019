package T15_Adapter;

public class Main {
	
    public static void main(String args[]) 
    { 
        Ajoneuvo auto = new Auto(); 
        Eläin hepo = new Hevonen(); 
        //Eläin wrapataan adapteriin.
        Eläin autoAdapteri = new AutoAdapteri(auto);
     
        System.out.println("Auto:");
        auto.aja();
        auto.tankkaa();
        
        System.out.println("Hevonen:");
        hepo.ratsasta();
        hepo.syötä();
        
        System.out.println("AutoAdapteri:");
        autoAdapteri.ratsasta();
        autoAdapteri.syötä();
    }
}
