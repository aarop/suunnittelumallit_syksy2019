package T15_Adapter;

public class Hevonen implements Eläin{

	@Override
	public void ratsasta() {
		System.out.println("KOPPOTI KOPPOTI Ratsastan hevosella KOPPOTI");
	}

	@Override
	public void syötä() {
		System.out.println("Syötin hevosen.");
	}

}
