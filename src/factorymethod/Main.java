/**
 * @author Severi Pälikkö
 */
package factorymethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus reksi = new Rehtori();
        AterioivaOtus talonmies = new Talonmies();
        opettaja.aterioi();
        reksi.aterioi();
        talonmies.aterioi();
    }
}
