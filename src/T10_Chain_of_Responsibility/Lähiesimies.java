package T10_Chain_of_Responsibility;

public class Lähiesimies extends Palkkapyyntö{

	@Override
	public void processRequest(int percentAmount) {
		if(percentAmount >2) {
			System.out.println("Lähiesimies delegoi pyynnön Yksikön päällikölle.");
			successor.processRequest(percentAmount);
		}
		else {
			System.out.println("Lähiesimies hyväksyy "+percentAmount+"% korotuksen palkkaanne.");
		}
		
	}

}
