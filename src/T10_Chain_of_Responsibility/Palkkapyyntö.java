package T10_Chain_of_Responsibility;

public abstract class Palkkapyyntö {

	protected Palkkapyyntö successor;
	public void setSuccessor(Palkkapyyntö successor) {
	this.successor = successor;
	}
	abstract public void processRequest(int percentAmount);
}
