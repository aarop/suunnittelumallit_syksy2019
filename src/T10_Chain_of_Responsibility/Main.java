package T10_Chain_of_Responsibility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

	public static void main(String[] args) {
		System.out.println("Anna palkankorotuksesi kokonaislukuna prosentteina\nesim. 2% on luku 2.");
		int paljon = Lue.kluku();
		Lähiesimies lMies = new Lähiesimies();
		YksikköPäällikkö yPäällikkö = new YksikköPäällikkö();
		Toimitusjohtaja TJ = new Toimitusjohtaja();
		lMies.setSuccessor(yPäällikkö);
		yPäällikkö.setSuccessor(TJ);
		lMies.processRequest(paljon);
		
		
	}
}
