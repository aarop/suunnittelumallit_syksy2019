package T10_Chain_of_Responsibility;

public class YksikköPäällikkö extends Palkkapyyntö{

	@Override
	public void processRequest(int percentAmount) {
		if(percentAmount >5) {
			System.out.println("Yksikön päällikkö delegoi pyynnön Toimitusjohtajalle.");
			successor.processRequest(percentAmount);
		}
		else {
			System.out.println("Yksikön päällikkö hyväksyy "+percentAmount+"% korotuksen palkkaanne.");
		}
	}

}