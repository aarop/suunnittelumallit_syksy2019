package T10_Chain_of_Responsibility;

public class Toimitusjohtaja extends Palkkapyyntö{

	@Override
	public void processRequest(int percentAmount) {
		System.out.println("TJ hyväksyy "+percentAmount+"% korotuksen palkkaanne.");

	}

}