package T12_Proxy;

import java.util.ArrayList;

public class ProxyExample {

	 public static void main(final String[] arguments) {
	        Image image1 = new ProxyImage("KUVA_1");
	        Image image2 = new ProxyImage("KUVA_2");
	        Image image3 = new ProxyImage("KUVA_3");
	        Image image4 = new ProxyImage("KUVA_4");
	        Image image5 = new ProxyImage("KUVA_5");
	        ArrayList<Image> lista = new ArrayList<Image>();
	        System.out.println("Albumissa olevat kuvat: ");
	        lista.add(image1);
	        lista.add(image2);
	        lista.add(image3);
	        lista.add(image4);
	        lista.add(image5);
	        for(Image i : lista) {
	        	i.showData();
	        }
	        boolean jatka = true;
	        while(jatka) {
		        System.out.println("Anna avattavan kuvan numero, alle 1 lopettaa ohjelman:");
		        int valinta = Lue.kluku();
		        if(valinta > lista.size()) {
		        	System.out.println("Ei keleppoo tommonen valinta.");
		        }
		        else if(valinta < 1) {
		        	jatka = false;
		        }
		        else {
		        	Image kuva = lista.get(valinta-1);
		        	kuva.displayImage();
		        }
	        }
	    }
}
