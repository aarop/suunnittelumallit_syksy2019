package T3_Composite;

import java.util.ArrayList;

public class Emolevy implements Laiteosa{

	private ArrayList<Laiteosa> lista;
	private double hinta = 100;
	private Laiteosa verkkokortti;
	private Laiteosa piiri;
	
	public Emolevy() {
		lista = new ArrayList<Laiteosa>();
		verkkokortti = new Verkkokortti();
		piiri = new Muistipiiri();
		lista.add(verkkokortti);
		lista.add(piiri);
	}
	
	
	@Override
	public double getHinta() {
		double e = 0;
		for(Laiteosa a : lista) {
			e += a.getHinta();
		}
		return hinta + e;
	}

}
