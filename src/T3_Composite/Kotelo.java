package T3_Composite;


public class Kotelo implements Laiteosa {

	private Laiteosa emo;
	private final double hinta = 50;
	
	public Kotelo() {
		emo = new Emolevy();
	}
	
	@Override
	public double getHinta() {
		return hinta + emo.getHinta();
	}

}
