package T3_Composite;

public class Main {

	public static void main(String[] args) {
		Laiteosa kotelo = new Kotelo();
		Laiteosa näyttis = new Näytönohjain();
		
		double hinta = kotelo.getHinta() + näyttis.getHinta();
		System.out.println("Kokoonpanon hinnaksi tuli "+hinta+" euroa.");
		
	}
}
