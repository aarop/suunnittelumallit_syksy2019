package T14_Builder;

import java.util.ArrayList;

public class Tarjoilija {

	private HesburgerBuilder hese;
	private McDonaldsBuilder mäkki;
	public void setBuilder(HesburgerBuilder b) {
		hese = b;
		mäkki = null;
	}
	public void setBuilder(McDonaldsBuilder b) {
		mäkki = b;
		hese = null;
	}
	public void createBurger() {
		if (mäkki != null || hese != null) {
			if(mäkki == null) {
				hese.buildBurger();
			}
			else {
				mäkki.buildBurger();
			}
		}
		else {
			System.out.println("Ei builderia asetettu.");
		}
	}
	public void getBurger() {
		if (mäkki != null || hese != null) {
			if(mäkki == null) {
				System.out.println("Hesehampparissasi on: ");
				for(String a : hese.getBurger()) {
					System.out.println(a);
				}
				
			}
			else {
				mäkki.getBurger().describeBurger();
			}
		}
		else {
			System.out.println("Ei builderia asetettu.");
		}
	}
	/*
	 * "Builderilla on getBurger()-metodi, joka palauttaa
	 * hampurilaisen sellaisena tietorakenteena, jona se on luotu."
	 * Eli eri palautustyyppi, eli ei voi käyttää yhteistä rajapintaa?
	 */
//	public Burger getBurger() {
//		return bb.getBurger();
//	}
//	public ArrayList<Burger> getBurger(){
//		return bb.getBurger();
//	}
	
	public static void main(String[] args) {
		
		Tarjoilija tarja = new Tarjoilija();
		HesburgerBuilder hese = new HesburgerBuilder();
		McDonaldsBuilder mäkki = new McDonaldsBuilder();
		System.out.println("Hei olen Tarja.");
		tarja.setBuilder(hese);
		tarja.createBurger();
		tarja.getBurger();
		
		tarja.setBuilder(mäkki);
		tarja.createBurger();
		tarja.getBurger();
	}
}
