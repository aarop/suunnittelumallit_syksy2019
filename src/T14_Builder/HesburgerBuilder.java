package T14_Builder;

import java.util.ArrayList;

public class HesburgerBuilder implements BurgerBuilder{

	private ArrayList<String> list;
	
	public HesburgerBuilder() {
		list = new ArrayList<String>();
	}
	public ArrayList<String> getBurger() {
		return list;
	}
	
	public void buildBurger() {
		list.add("Nahkea salaatinlehti");
		list.add("Epäilyttävä pihvi");
		list.add("Hesen kastike");
	}

}
