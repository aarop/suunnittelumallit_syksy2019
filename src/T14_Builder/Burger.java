package T14_Builder;

public class Burger {

	private Salaatti salaatti;
	private Pihvi pihvi;
	private Kastike kastike;
	
	public void build() {
		salaatti = new Salaatti();
		pihvi = new Pihvi();
		kastike = new Kastike();
	}
	
	public void describeBurger() {
		System.out.println("McDonalds-Hampurilaisen ainesosat: ");
		System.out.println(salaatti.toString());
		System.out.println(pihvi.toString());
		System.out.println(kastike.toString());
	}
}
