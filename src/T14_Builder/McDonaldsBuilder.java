package T14_Builder;

public class McDonaldsBuilder implements BurgerBuilder{

	private Burger burger;
	
	public void buildBurger() {
		burger = new Burger();
		burger.build();
	}
	
	public Burger getBurger() {
		return burger;
	}
	
}
